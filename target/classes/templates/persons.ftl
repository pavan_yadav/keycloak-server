<#import "/spring.ftl" as spring>

<html>
    <h1>Welcome Page</h1>
<#--    <div style="display: none">-->
<#--        name=${name}-->
<#--    </div>-->
<#--    <ul>-->
<#--        <#list persons as person>-->
<#--            <li>${person}</li>-->
<#--        </#list>-->
<#--    </ul>-->

    <h4>Assigned Roles</h4>
    <ul>
        <#list roles as role>
            <li>
                ${role}
            </li>
        </#list>
    </ul>

    <h4>Visible Page to a user</h4>

        <#list roles as role>

            <div style="display: none">
                <h3>User Role: role=${role}</h3>
            </div>

            <#if role=="ROLE_admin">
                <button type="button">Document UPLOAD</button>
            </#if>

            <#if role=="ROLE_moderator">
                <p>View Document Page</p>
            </#if>

            <#if role=="ROLE_user">
                <p>User Access</p>
            </#if>

            <#if role=="ROLE_offline_access">
                <p>Offline Access</p>
            </#if>

        </#list>


    <h1>Attribute Configured for Roles</h1>

    mobile=${mobile} </br>
        <#if mobile == "123">
            Attribute Matches
        <#else>
            Attribute Mismatch
        </#if>

    <br>
    <a href="/logout">Logout</a>
</html>