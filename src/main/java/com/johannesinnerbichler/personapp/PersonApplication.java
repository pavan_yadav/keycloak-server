package com.johannesinnerbichler.personapp;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.adapters.KeycloakConfigResolver;
import org.keycloak.adapters.springboot.KeycloakSpringBootConfigResolver;
import org.keycloak.adapters.springsecurity.KeycloakConfiguration;
import org.keycloak.adapters.springsecurity.account.KeycloakRole;
import org.keycloak.adapters.springsecurity.authentication.KeycloakAuthenticationProvider;
import org.keycloak.adapters.springsecurity.config.KeycloakWebSecurityConfigurerAdapter;
import org.keycloak.representations.IDToken;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.mapping.SimpleAuthorityMapper;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.web.authentication.session.RegisterSessionAuthenticationStrategy;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;
import java.lang.reflect.Field;
import java.net.URI;
import java.security.Principal;
import java.util.*;

@SpringBootApplication
public class PersonApplication {

    public static void main(String[] args) {
        SpringApplication.run(PersonApplication.class, args);
    }
}

@Configuration
@EnableCaching
class ApplicationConfiguration {
    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    @Value("${route.paths}")
    private String  merchantPaths;

    @Bean
    public HashSet<String> routePathMapping(){
        HashSet<String> hashPath = new HashSet<>();
        String paths = merchantPaths;
        String[] pathList = paths.split(",");
        System.out.println("paths: "+pathList);
        for(String s : pathList)
            hashPath.add(s);
        return hashPath;
    }
}

class Realm{
    @JsonProperty("realm_access")
    public Roles realmAccess;
}
class Roles{
    @JsonProperty("roles")
    public String[] roles;
}

@Controller
class PersonController {

    @Autowired
    private HashSet<String> routePathMapping;

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping(path = "/")
    public String getIndex() {
        return "index";
    }

    @GetMapping(path = "/check")
    public ResponseEntity<String> check(@RequestParam(value = "client_id") String clientId,
                                        @RequestParam(value = "username") String username,
                                        @RequestParam(value = "client_secret") String clientSecret,
                                        @RequestParam(value = "token") String token,
                                        @RequestParam(value = "endpoint") String endPoint,HttpServletRequest request){

        System.out.println("requestURI: " + request.getRequestURI());

        try {
            String url="http://localhost:8080/auth/realms/xyz/protocol/openid-connect/userinfo";
            URI uri = new URI( url);
            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization","bearer "+token);

            HttpEntity entity = new  HttpEntity<>(headers);

            ResponseEntity<Map> response = restTemplate.exchange(uri, HttpMethod.GET,entity,Map.class);
            System.out.println("GET Response "+response);

            if(response.getStatusCode().value() == 200) {

                url="http://localhost:8080/auth/realms/xyz/protocol/openid-connect/token/introspect";

                headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

                MultiValueMap<String,String> map = new LinkedMultiValueMap<>();
                map.add("client_id",clientId);
                map.add("username",username);
                map.add("client_secret",clientSecret);
                map.add("token",token);

                entity = new HttpEntity(map,headers);

                ResponseEntity<Realm> response2 = restTemplate.exchange(url, HttpMethod.POST,entity,Realm.class);

                System.out.println("POST Response "+response2);
                System.out.println("Body "+response2.getBody());

                System.out.println("obj "+response2.getBody().realmAccess.roles.length);

                String[] userRoles = response2.getBody().realmAccess.roles;
                System.out.println("userRoles "+userRoles[0]);

                if(response2.getStatusCode().value() == 200) {

                    for (String role : userRoles){
                        if(routePathMapping.contains(endPoint+":"+role)) {
                            System.out.println("*************SUCCESS***********");
                            return new ResponseEntity<>("Access Granted", HttpStatus.OK);
                        }
//                        if(role.equals("user"))
//                            return new ResponseEntity<>("Access Granted", HttpStatus.OK);
                    }
                    return new ResponseEntity<>("You don't have access for this resource", HttpStatus.FORBIDDEN);


                }
                return new ResponseEntity<>("Access Granted", HttpStatus.OK);
            }

        } catch (Exception ex) {
            System.out.println("exception");
            return new ResponseEntity<>("Token was not recognised", HttpStatus.BAD_REQUEST);
        }
        System.out.println(new ResponseEntity<>("You don't have access for this resource", HttpStatus.FORBIDDEN));
        return new ResponseEntity<>("You don't have access for this resource", HttpStatus.FORBIDDEN);
    }

    @GetMapping(path = "/persons")
    @PreAuthorize("hasRole('admin') or hasRole('moderator') or hasRole('normal')")
    public String getPersons(Model model, HttpServletRequest request) {
        model.addAttribute("persons", Arrays.asList("John", "David", "Peter"));

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        KeycloakPrincipal userDetails = (KeycloakPrincipal) authentication.getPrincipal();

        Collection<? extends GrantedAuthority> grantedAuthorities = authentication.getAuthorities();
        System.out.println("authorities "+grantedAuthorities);

//        Object obj[] = grantedAuthorities.toArray();
        List list = (List) grantedAuthorities;

        System.out.println("obj "+list);
        model.addAttribute("roles",list);

        for (GrantedAuthority g: grantedAuthorities){
            System.out.println(g);
        }

        System.out.println("authentication "+authentication);
        System.out.println("userDetails "+userDetails);


        if (userDetails instanceof KeycloakPrincipal) {

            IDToken idToken = userDetails.getKeycloakSecurityContext().getIdToken();

            Map<String, Object> otherClaims = idToken.getOtherClaims();
            System.out.println("idToken "+idToken);

            System.out.println("size "+otherClaims.size());
            System.out.println("otherClaims "+otherClaims);
            System.out.println("name "+idToken.getName());
            System.out.println("email "+idToken.getEmail());

            model.addAttribute("mobile","");
            String yourClaim;
//            model.addAttribute("xyz","12");

            if (otherClaims.containsKey("mobile")) {
                yourClaim = String.valueOf(otherClaims.get("mobile"));
                System.out.println("claim "+yourClaim);
                model.addAttribute("mobile",yourClaim);
            }
        }else {
            throw new RuntimeException("Exception Occurred");
        }

        return "persons";
    }



    @GetMapping(path = "/logout")
    public String logout(HttpServletRequest request) throws ServletException {
        request.logout();
        return "/";
    }

    @GetMapping(path = "/merchant")
    public String merchant(HttpServletRequest request) throws ServletException {

        return "/";
    }
}

@KeycloakConfiguration
class SecurityConfig extends KeycloakWebSecurityConfigurerAdapter {

    /**
     * Registers the KeycloakAuthenticationProvider with the authentication manager.
     */
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        KeycloakAuthenticationProvider keycloakAuthenticationProvider = keycloakAuthenticationProvider();

        // adding proper authority mapper for prefixing role with "ROLE_"
        keycloakAuthenticationProvider.setGrantedAuthoritiesMapper(new SimpleAuthorityMapper());

        auth.authenticationProvider(keycloakAuthenticationProvider);
    }

    /**
     * Provide a session authentication strategy bean which should be of type
     * RegisterSessionAuthenticationStrategy for public or confidential applications
     * and NullAuthenticatedSessionStrategy for bearer-only applications.
     */
    @Bean
    @Override
    protected SessionAuthenticationStrategy sessionAuthenticationStrategy() {
        return new RegisterSessionAuthenticationStrategy(new SessionRegistryImpl());
    }

    /**
     * Use properties in application.properties instead of keycloak.json
     */
    @Bean
    public KeycloakConfigResolver KeycloakConfigResolver() {
        return new KeycloakSpringBootConfigResolver();
    }

    /**
     * Secure appropriate endpoints
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        super.configure(http);
        http.authorizeRequests()
                .antMatchers("/persons*").authenticated()// only user with role user are allowed to access
                .anyRequest().permitAll();
    }
}

